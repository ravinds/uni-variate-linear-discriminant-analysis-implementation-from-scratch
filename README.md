# Uni-Variate Linear Discriminant Analysis - Implementation from Scratch

**Summary:**

Through this algorithm, we want to transform our dataset such that the "between class" variance is maximized relative to the "within class" variance. We basically want to cluster data points into groups.

We have 3 classes in our target variable namely, 0, 1 and 2.

LDA makes two assumptions i.e.,

1.) The data is gaussian. I constructed the x variable from a standard gaussian distribution.

2.) Each predictor variable has the same variance. Since there is just one x variable, this assumption is trivially confirmed.

The discriminant function given below gives the discriminant value for an input for each class. This equation is valid for only univariate LDA.

D_k(x) = x.(mu_k).(sigma^2) - ((mu_k)^2)/(2.sigma^2) + ln(P(k))

where,

mu_k = mean of x values in class k,

sigma^2 = (1/(n - k)).Sum of (x_i - mean(x))^2, here n = total number of records, and k = number of classes,

P(k) = proportion of class k records.

The input x is classified as the class which has the highest discriminant value.

*Comparison with sklearn LDA*:

Note: I implemented Stratified 5-fold cross validation for Accuracy.

|     | My implementation | SkLearn |
| --- | --- | --- | 
| Mean Stratified 5-fold Accuracy | 0.343 | 0.288 |
|  |
Computation Time | 60.0464 | 0.03496 |

I found out that even though the accuracy of my implementation is better than sklearn, but the computation time is many fold higher than sklearn. I believe this is because of the numerous functions I created. Also, my function definition allows for the general datasets (with more than 1 predictor variable) and calculating mean for various x-variables separately, this might have added to the computation time.

**Reflection:**

*Useful Takeaways*:

1.) I could implement my own code for the LDA and didn't depend on sklearn. This helped me understand the inner mechanism of the algorithm.

2.) I learned about stratified cross validation and its importance in target variable with finite number of classes.

*Things that surprised me*:

I could appreciate the amount of time and energy that would be required to implement the general LDA and not just univariate dataset and few classes in target variable.

*Things that caused difficulty and challenged me*:

Outside of the requirements for this assignment, I am working on building up my own LDA library that would work for the generic dataset and target varaible.


