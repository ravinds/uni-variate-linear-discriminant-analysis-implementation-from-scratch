#!/usr/bin/env python
# coding: utf-8

# In[ ]:

# library_file

# Importing all the required Python libraries.

import numpy as np
import statistics
from sklearn.metrics import accuracy_score
import sklearn.model_selection as sms
import time


# ClassMean function:

# This function returns the mean vector (mean values of different predictor variables) of a particular class (k).

def ClassMean(data,k):
    b = []
    for i in range(0,data.shape[1]-1):
        b.append(0)
        
    for j in range(0,data.shape[1]-1):
        sum = 0 
        for i in range(0,data[data[data.columns[data.shape[1]-1]] == k].shape[0]):
            sum = sum + data[data[data.columns[data.shape[1]-1]] == k].iat[i,j]
        b[j] = sum/data[data[data.columns[data.shape[1]-1]] == k].shape[0]
        
    return b


# DataMean function:

# This function returns the mean vector (mean values of different predictor variables) of the whole data.

def DataMean(data):
    b = []
    for i in range(0,data.shape[1]-1):
        b.append(0)
    
    for j in range(0,data.shape[1]-1):
        sum = 0
        for i in range(0,data.shape[0]):
            sum = sum + data.iat[i,j]
        b[j] = sum/data.shape[0]
        
    return b


# SigmaSquared function:

# This function returns the sigma^2. Works only for univariate LDA.

def SigmaSquared(data):
    sum = 0
    for i in range(0,data.shape[0]):
        sum = sum + (data.iat[i,0] - DataMean(data))**2
    return sum/((data.shape[0]-3))


# Discriminant function:

# This function returns the discriminant value of univariate x value for class (k). Works only for univariate LDA.

def Discriminant(data,k,x):
    return np.dot(x,ClassMean(data,k))*(1/SigmaSquared(data)) - np.dot(ClassMean(data,k),ClassMean(data,k))/(2*SigmaSquared(data)) + np.log((data[data[data.columns[1]] == k].shape[0])/data.shape[0])


# ClassDecider function:

# This function returns the predicted class (0 or 1 or 2) for a univariate x value.

def ClassDecider(data,x):
    maximum = max(Discriminant(data,0,x),Discriminant(data,1,x),Discriminant(data,2,x))
    if(Discriminant(data,0,x) == maximum):
        return 0
    if(Discriminant(data,1,x) == maximum):
        return 1
    if(Discriminant(data,2,x) == maximum):
        return 2

    
# Classifier function:

# This function returns the predicted class vector for the univariate x vector.

def Classifier(data,X):
    b = []
    for i in range(0,X.shape[0]):
        b.append(0)
        
    for i in range(0,X.shape[0]):
        b[i] = ClassDecider(data,X.iat[i])
    
    return b

    
# AccuracyScore function:

# This function returns the mean 5-fold accuracy score for the test data. It also returns the individual accuracy scores for each fold.

def AccuracyScore(data,k):
    kf = sms.StratifiedKFold(n_splits = k, shuffle = True)
    scores = []
    for i in range(k):
        result = next(kf.split(data,data[data.columns[1]]), None)
        train = data.iloc[result[0]]
        test = data.iloc[result[1]]
        accu_score = accuracy_score(test[test.columns[1]],Classifier(train,test[test.columns[0]]))
        scores.append(accu_score)
                                        
    return scores, np.mean(scores)


# ComputationTime function:

# This function returns the computation time for computing the 5-fold accuracy values.

def ComputationTime(data,k):
    time_start = time.perf_counter()
    kf = sms.StratifiedKFold(n_splits = k, shuffle = True)
    scores = []
    for i in range(k):
        result = next(kf.split(data,data[data.columns[1]]), None)
        train = data.iloc[result[0]]
        test = data.iloc[result[1]]
        accu_score = accuracy_score(test[test.columns[1]],Classifier(train,test[test.columns[0]]))
        scores.append(accu_score)
                                        
    time_elapsed = (time.perf_counter() - time_start)
    
    return(time_elapsed)





