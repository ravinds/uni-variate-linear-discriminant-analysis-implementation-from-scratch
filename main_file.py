#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Importing all the required Python libraries and LDA (library file).

import pandas as pd
import numpy as np
import random
import statistics
import time
import sklearn.model_selection as sms
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
import LDA


# In[2]:


# Generating X attribute.

# The X attribute is generated from a standard gaussian distribution, so to fulfill the first assumption of LDA algortihm.

mu, sigma = 0, 1
np.random.seed(30)
x = np.random.normal(mu, sigma, 100)

# Generating Y (with 3 classes).
random.seed(30)
y = []
for i in range(0,100):
    n = random.choice([0,1,2])
    y.append(n)

# Joining X and Y into a data frame.
data = pd.DataFrame({'X': x,'Class':y})


# In[ ]:


data.head()


# # Comparing Mean 5-fold Accuracy

# In[3]:


# Fitting the LDA and finding the accuracy values using AccuracyScore function.

LDA.AccuracyScore(data,5)


# In[11]:


# Fitting the sklearn LDA.

lda = LinearDiscriminantAnalysis(n_components=1)
lda.fit(pd.DataFrame(x),y)

# Accuracy values for sklearn LDA as benchmark implementation.

cv_accuracy_scores_rf = sms.cross_val_score(lda, pd.DataFrame(x), y, cv = sms.StratifiedKFold(n_splits=5, shuffle=True),scoring='accuracy')
print(cv_accuracy_scores_rf)
print("Mean 5-Fold Accuracy: {}".format(np.mean(cv_accuracy_scores_rf)))


# # Comparing Computation Time

# In[12]:


# Finding the Computation Time using ComputationTime function.

LDA.ComputationTime(data,5)


# In[14]:


# Computation Time for sklearn LDA as benchmark implementation.

time_start = time.perf_counter()

lda = LinearDiscriminantAnalysis(n_components=1)
lda.fit(pd.DataFrame(x),y)
cv_accuracy_scores_rf = sms.cross_val_score(lda, pd.DataFrame(x), y, cv = sms.StratifiedKFold(n_splits=5, shuffle=True),scoring='accuracy')

time_elapsed = (time.perf_counter() - time_start)
time_elapsed


# In[ ]:




